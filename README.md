# Data Processing with Vector Database

## 1. Install and Run Qdrant

Qdrant stands out as a database of choice because it provides specially optimized storage and efficient similarity search capabilities for vector data, making it ideally suited to support the needs of modern AI and machine learning applications.

To install and run qdrant, we just need to open a terminal and type:

```
docker pull qdrant/qdrant
docker run -p 6333:6333 -p 6334:6334 qdrant/qdrant
```

And we can see the qdrant server is running from the docker:

![docker screenshot](https://gitlab.com/estrellas939/week7proj/-/raw/main/pic/docker.png?ref_type=heads&inline=false)

## 2. Create a New Rust Project
Use the command `cargo new week7proj` and `cd week7proj` to go to the directory.

## 3. Add Dependencies

Under file `Cargo.toml`, we need to add dependencies for this project.

```
[dependencies]
reqwest = { version = "0.11.26", features = ["json"] }
serde = { version = "1.0.197", features = ["derive"] }
serde_json = "1.0.114"
tokio = { version = "1.36.0", features = ["full"] }
plotters = "0.3.5"
qdrant-client = "1.8.0"
```

## 4. Create a Collection 

Before create vectores and insert them into the database, qdrant requires us to create a collection first. And all the following instructions will operate based on this collection.
We can simpliy create a collection via terminal or in the `main.rs` using code
```
curl -X POST 'http://localhost:6333/collections' -H 'Content-Type: application/json' -d '
{
  "name": "my_2d_collection",
  "vector_size": 2,
  "distance": "Cosine"
}'
```
We shall the following result:
```
{"result":true,"status":"ok","time":0.187209447}
```

We can also run the command `curl -X GET 'http://localhost:6333/collections'` to check the collections we build.
```
{"result":{"collections":[{"name":"my_2d_collection"}]},"status":"ok","time":3.96e-6}
```

To verify the colleciton, we can open `http://localhost:6333/dashboard`.
![screenshot dashboard](https://gitlab.com/estrellas939/week7proj/-/raw/main/pic/collection.png?ref_type=heads&inline=false)

## 5. Create Vectors
In the file `main.rs`, we need to create vectors. Here I create a two-dimensional vector just for demonstratation.

```
// Create data
    let points = vec![
    PointStruct::new(
        1,
        vec![0.1, 0.2], // a two-dimensional vector
        json!(
            {"city": "Berlin"}
        )
        .try_into()
        .unwrap(),
    ),
    PointStruct::new(
        2,
        vec![0.3, 0.4],
        json!(
            {"city": "London"}
        )
        .try_into()
        .unwrap(),
    ),
    ];
```

## 6. Insert Vectors to Collection
Next we need to insert vectors to the collection.
```
// Insert data
    let qdrant_client = QdrantClient::from_url("http://localhost:6334").build()?;

    qdrant_client
        .upsert_points("my_2d_collection".to_string(), None, points, None)
        .await?;
```

To verify the data, we can open `http://localhost:6333/dashboard#/collections/my_2d_collection`
![screenshot collection](https://gitlab.com/estrellas939/week7proj/-/raw/main/pic/points.png?ref_type=heads&inline=false)

## 7. Query
Next is the implementation of query function.
```
// Query
    let search_payload = SearchPayload {
        vector: vec![0.1, 0.2],
        top: 2,
    };

    let res = client.post("http://localhost:6333/collections/my_2d_collection/points/search")
        .json(&search_payload)
        .send()
        .await?;

    println!("Search Response: {:?}", res);
```
Type `cargo run` in the terminal to see results:
```
   Compiling week7proj v0.1.0 (/Users/wanghaotian/week7proj)
    Finished dev [unoptimized + debuginfo] target(s) in 4.69s
     Running `target/debug/week7proj`
Collection already exists
Search Response: Response { url: Url { scheme: "http", cannot_be_a_base: false, username: "", password: None, host: Some(Domain("localhost")), port: Some(6333), path: "/collections/my_2d_collection/points/search", query: None, fragment: None }, status: 200, headers: {"content-length": "181", "content-type": "application/json", "vary": "Origin, Access-Control-Request-Method, Access-Control-Request-Headers", "date": "Tue, 19 Mar 2024 17:01:38 GMT"} }
Search results: Object {"result": Array [Object {"id": Number(1), "payload": Null, "score": Number(0.99999994), "vector": Null, "version": Number(9)}, Object {"id": Number(2), "payload": Null, "score": Number(0.9838699), "vector": Null, "version": Number(9)}], "status": String("ok"), "time": Number(0.00517367)}
```

## 8. Visualization
Due to the search response is a response body formatted in JSON, we need to analyze the results and present the information we want to know. 
```
// Visualize results
    if res.status().is_success() {
        let search_results: serde_json::Value = res.json().await.unwrap();
        let mut scores = Vec::new();
        println!("Search results: {:?}", search_results);
        println!("Visualization Details:");
        if let Some(results) = search_results["result"].as_array() {
        //println!("entering the if statement at line 128");
        for hit in results {
            //println!("i am at the loop!");
            let id = hit["id"].as_i64().unwrap(); // Get the ID of the point
            let score = hit["score"].as_f64().unwrap_or(0.0) as f32; // Get the score of the point and convert it to f32
            scores.push((id, score)); // Add ID and score to array  
            println!("ID: {}, Score: {:?}", id, score);
        }
    } else {
        println!("'result' is not an array or not present");
    }
```
Here we parse the `result` field from the search response and send back the deatil of the information after running `cargo run`:
```
Visualization Details:
ID: 1, Score: 0.99999994
ID: 2, Score: 0.9838699
```
Now it can return the ID and similarity between vector and query object, which is more intuitive. At the same time, a picture called `score_visualization.png` with similarity score will be generated and stored under the directory.
![screenshot result](https://gitlab.com/estrellas939/week7proj/-/raw/main/pic/bash.png?ref_type=heads&inline=false)

## 9. Future Insights
Because response body cannot return the specific payload and value size of the vector, the query results need to be further improved, and the functions in the visualization part need to be changed to provide more detailed results. At the same time, higher-dimensional vectors can be input to test the program.
