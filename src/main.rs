use reqwest::Client;
//use reqwest::Error;
use serde::Serialize;
use serde_json::json;
use tokio;
use plotters::prelude::*;
use qdrant_client::qdrant::PointStruct;
use qdrant_client::prelude::QdrantClient;

#[derive(Serialize)]
struct Vector {
    vector: Vec<f32>,
}

#[derive(Serialize)]
struct Payload {
    vectors: Vec<Vector>,
    ids: Vec<u32>,
}

#[derive(Serialize)]
struct SearchPayload {
    vector: Vec<f32>,
    top: u32,
}

fn visualize(scores: &[(i64, f32)]) -> Result<(), Box<dyn std::error::Error>> {
    let root = BitMapBackend::new("score_visualization.png", (640, 480)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("Search Result Scores", ("sans-serif", 40))
        .build_cartesian_2d(0..scores.len() as i32, 0f32..1f32)?;

    chart.configure_mesh().draw()?;

    chart.draw_series(scores.iter().enumerate().map(|(idx, &(_id, score))| {
        Circle::new((idx as i32, score), 5, RED.filled())
    }))?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = Client::new();

    // Check if the collection exists
    let collection_check_response = client.get("http://localhost:6333/collections/my_2d_collection")
        .send()
        .await?;

    // If the collection does not exist, create the collection
    if collection_check_response.status().is_client_error() {
        println!("Collection does not exist, creating a new one...");
        let collection_creation_response = client.post("http://localhost:6333/collections")
            .json(&json!({
                "name": "my_2d_collection",
                "vector_size": 2,
                "distance": "Cosine"
            }))
            .send()
            .await?;

        if collection_creation_response.status().is_success() {
            println!("Collection created successfully");
        } else {
            println!("Failed to create collection: {:?}", collection_creation_response.text().await?);
        }
    } else {
        println!("Collection already exists");
    }

    // Create data
    let points = vec![
    PointStruct::new(
        1,
        vec![0.1, 0.2], // a two-dimensional vector
        json!(
            {"city": "Berlin"}
        )
        .try_into()
        .unwrap(),
    ),
    PointStruct::new(
        2,
        vec![0.3, 0.4],
        json!(
            {"city": "London"}
        )
        .try_into()
        .unwrap(),
    ),
    ];

    // Insert data
    let qdrant_client = QdrantClient::from_url("http://localhost:6334").build()?;

    qdrant_client
        .upsert_points("my_2d_collection".to_string(), None, points, None)
        .await?;

    //dbg!(operation_info);

    // Query
    let search_payload = SearchPayload {
        vector: vec![0.1, 0.2],
        top: 2,
    };

    let res = client.post("http://localhost:6333/collections/my_2d_collection/points/search")
        .json(&search_payload)
        .send()
        .await?;

    println!("Search Response: {:?}", res);

    
    // Visualize results
    if res.status().is_success() {
        let search_results: serde_json::Value = res.json().await.unwrap();
        let mut scores = Vec::new();
        println!("Search results: {:?}", search_results);
        println!("Visualization Details:");
        if let Some(results) = search_results["result"].as_array() {
        //println!("entering the if statement at line 128");
        for hit in results {
            //println!("i am at the loop!");
            let id = hit["id"].as_i64().unwrap(); // Get the ID of the point
            let score = hit["score"].as_f64().unwrap_or(0.0) as f32; // Get the score of the point and convert it to f32
            scores.push((id, score)); // Add ID and score to array  
            println!("ID: {}, Score: {:?}", id, score);
        }
    } else {
        println!("'result' is not an array or not present");
    }
        
        // Call visualization functions to handle possible errors
        if let Err(e) = visualize(&scores) {
            eprintln!("Visualization error: {}", e);
        }
    } else {
        eprintln!("Search request failed with status: {}", res.status());
    }
    Ok(())
}
